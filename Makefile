
#gene_synonym.bdb: make_synonym_dic.pl; $<

all: gene2go.gz gene_info.gz taxdump.tar.gz \
	scientific_names.txt species_list.txt taxid_list.txt

NCBI_GENE_FTP := ftp://ftp.ncbi.nih.gov/gene/DATA
NCBI_TAXONOMY_FTP := ftp://ftp.ncbi.nih.gov/pub/taxonomy
WGET := wget -nd -nv

gene2go.gz: ; ${WGET} ${NCBI_GENE_FTP}/$@
gene_info.gz: ; ${WGET} ${NCBI_GENE_FTP}/$@
taxdump.tar.gz: ; ${WGET} ${NCBI_TAXONOMY_FTP}/$@


scientific_names.txt: names.dmp Makefile
	grep 'scientific name' $< | cut -f1,3,5 | tr -d \' | grep -Ev 'Plasmid|sp\.' | sort -f -k2 >$@

species_list.txt: ../SPECIES
	tr '_' ' ' $< >$@

taxid_list.txt: species_list.txt scientific_names.txt
	grep -Ff $^ | egrep -v '[/()]| of | x ' >$@


taxid_list2.txt: species_list.txt scientific_names.txt
	x='\(' `tr '\n' '|' < $<` '\)'
	grep -E $^ | egrep -v '[/()]| of | x ' >$@

species_color.txt:
	grep --color=always -Ff species.txt scientific_names.txt 

