CREATE OR REPLACE VIEW taxonomic_node_parent AS
  SELECT * FROM tax_names
    NATURAL JOIN taxonomic_nodes
  WHERE name_class = 'scientific name';
;
--CREATE INDEX ON taxonomic_node_parent(tax_id);
--CREATE INDEX ON taxonomic_node_parent(name_class);
CREATE INDEX ON tax_names(tax_id) WHERE name_class='scientific name';

--DROP FUNCTION taxon_rank(TEXT,TEXT);
CREATE OR REPLACE FUNCTION taxon_rank(sp_name TEXT, t_rank TEXT)
RETURNS TABLE(tax_id INT, taxon TEXT)
LANGUAGE sql AS $$
WITH RECURSIVE r AS (
  SELECT * FROM taxonomic_node_parent
    WHERE name_txt = sp_name
 UNION
  SELECT t.* FROM taxonomic_node_parent t, r
    WHERE t.tax_id = r.parent_tax_id
)
SELECT tax_id, name_txt
  FROM r WHERE rank = t_rank;
$$;

COMMENT ON FUNCTION taxon_rank(TEXT, TEXT)
  IS 'Get phylum name from species name';


/*
   Table "public.tax_names"
  Column     |  Type   | Modifiers
-------------+---------+-----------
 tax_id      | integer | not null
 name_txt    | text    | not null
 unique_name | text    |
 name_class  | text    | not null

   Table "public.taxonomic_nodes"
    Column     |  Type   | Modifiers
---------------+---------+-----------
 tax_id        | integer | not null
 parent_tax_id | integer |
 rank          | text    |
*/
