#!/usr/bin/perl -w
#

use strict ;
use BerkeleyDB ;

my $filename = "gene_synonym.bdb" ;
unlink $filename ;

tie my %h, 'BerkeleyDB::Btree',
       -Filename   => $filename,
       -Flags      => DB_CREATE
     or die "Cannot open $filename: $! $BerkeleyDB::Error\n" ;


open my $fh, "zcat gene_info.gz | grep -v '^#' | cut -f2,3,5,9,11,12 |";


while (<$fh>) {
    my ($id, $name, $synonym, $desc, $hgnc_id, $hgnc_desc) = split /\t/;
    $synonym eq '-' and next;
    if ($desc ne $hgnc_desc and $hgnc_id ne '-' || $hgnc_desc ne '-') {
	$desc .= " {$hgnc_id|$hgnc_desc}";
    }
    my @synonym = split /\|/, $synonym;
    map { $h{$_}="$id|$name|$desc" } @synonym;
}

close $fh;

untie %h;
