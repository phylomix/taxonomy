

DROP TABLE gene_info;
CREATE TABLE if not exists gene_info (
  tax_id       integer
  , "GeneID"   integer primary key
  , "Symbol"   text not null
  , "LocusTag" text
  , "Synonyms" text
  , "dbXrefs"  text
  , chromosome text
  , map_location text
  , description text
  , type_of_gene text
  , Symbol_from_nomenclature_authority text
  , Full_name_from_nomenclature_authority text
  , Nomenclature_status text
  , Other_designations text
  , Modification_date date
);

\copy gene_info FROM PROGRAM 'zcat gene_info.gz|sed "1d;s/\\\/\\\\\\\\/g"' WITH NULL '-';

CREATE INDEX ON gene_info("Symbol");
CREATE INDEX ON gene_info("Synonyms");
CREATE INDEX ON gene_info("dbXrefs");
CREATE INDEX ON gene_info USING GIN(to_tsvector('english',
	description || ' '|| full_name_from_nomenclature_authority ));
CREATE INDEX ON gene_info USING GIN(to_tsvector('english',
	lower(description || ' '|| full_name_from_nomenclature_authority )));
CREATE INDEX ON gene_info USING GIN(to_tsvector('english',
	"Symbol" || ' ' || "Synonyms" || ' '|| symbol_from_nomenclature_authority ));
CREATE INDEX ON gene_info USING GIN(to_tsvector('english',
	lower("Symbol" || ' ' || "Synonyms" || ' '|| symbol_from_nomenclature_authority )));

