#!/usr/bin/perl

use XML::XPath;
use XML::XPath::XMLParser;


my $fetcher = 'curl -s http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy\&id=';

$xmlres = join "", <DATA>;

sub str($ ) { 
    my @n;
    for my $x ( $_[0]->get_nodelist ) {
        push @n, XML::XPath::XMLParser::as_string( $x );
    }
    return @n;
}

while (<>) {
    chomp;
    my ($taxid, $name) = split "\t";
    my $xmlres = `$fetcher$taxid`;
    print STDERR $fetcher.$taxid; print "\n";
    my $xp = XML::XPath->new(xml => $xmlres);
    my $nodeset = $xp->find('//Taxon[Rank="phylum"]');
    for my $node ( $nodeset->get_nodelist ) {
    	$id = $node->find('//Taxon[Rank="phylum"]/TaxId/text()');
    	$na = $node->find('//Taxon[Rank="phylum"]/ScientificName/text()');
	print join "\t", $taxid, $name, str($id), str($na);
	print "\n";
    }
    $xp->clear_namespaces();
}

__END__
<literature>
<Taxon>
	<TaxId>7711</TaxId>
	<ScientificName>Chordata</ScientificName>
	<Rank>phylum</Rank>
</Taxon>
</literature>
