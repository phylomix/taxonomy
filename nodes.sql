CREATE TABLE taxonomic_nodes (
       tax_id	        int primary key
       ,parent tax_id   int referencing taxonomic_nodes deferred
       ,rank            text   -- rank of this node (superkingdom, kingdom, ...)
);

CREATE 