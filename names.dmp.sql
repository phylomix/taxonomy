

DROP TABLE public.tax_names;
CREATE TABLE if not exists public.tax_names(
  tax_id integer NOT NULL
  , name_txt text NOT NULL
  , unique_name text
  , name_class  text NOT NULL
);

\copy tax_names FROM PROGRAM 'cut -f1,3,5,7 names.dmp';

CREATE INDEX ON tax_names(tax_id);
CREATE INDEX ON tax_names(name_class);
CREATE INDEX ON tax_names(name_txt) WHERE name_class='authority';
CREATE INDEX ON tax_names(unique_name);
