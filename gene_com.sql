--CREATE OR REPLACE VIEW public.gene_view AS
--DROP VIEW public.gene_view;
CREATE OR REPLACE VIEW public.gene_view AS
SELECT "Species", gene_info.* 
  FROM (SELECT tax_id, string_agg(name_txt, '; ') AS "Species"
          FROM tax_names
	  WHERE name_class='scientific name'
	     OR name_class='genbank common name'
	  GROUP BY tax_id) t
  NATURAL JOIN gene_info
;


/*
CREATE OR REPLACE VIEW public.gene_view AS
SELECT name_txt AS "Species", gene_info.* 
  FROM tax_names
  NATURAL JOIN gene_info
  WHERE name_class='scientific name' 
     OR name_class='genbank common name'
;
*/
