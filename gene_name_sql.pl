#!/usr/bin/perl -w
#

use DBI;

my $dbname = 'phylomix';
my $user = 'phylomix';
my $pass = 'password';

$dbh = DBI->connect("dbi:Pg:dbname=$dbname", $user, $pass, {AutoCommit => 0});
# The AutoCommit attribute should always be explicitly set

# For some advanced uses you may need PostgreSQL type values:
use DBD::Pg qw(:pg_types);

# For asynchronous calls, import the async constants:
use DBD::Pg qw(:async);

my $sth = $dbh->prepare( 'SELECT "Symbol" FROM gene_info WHERE "Synonyms" ~ ?');

open my $fh, "|sort --ignore-case" or die;
select $fh;
for my $key (@ARGV) {
    $sth->execute($key);
    my $ary_ref = $sth->fetchall_arrayref;
    print map "@$_\n", @$ary_ref;
}
close $fh;
$dbh->disconnect;
